# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import imagekit.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0012_auto_20150727_0310'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='lokafyer',
            name='postal_code',
        ),
        migrations.RemoveField(
            model_name='lokafyer',
            name='standard_rate',
        ),
        migrations.AlterField(
            model_name='lokafyer',
            name='photo',
            field=imagekit.models.fields.ProcessedImageField(null=True, upload_to=b'form_photos'),
            preserve_default=True,
        ),
    ]
