# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import image_cropping.fields


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0017_auto_20150813_0943'),
    ]

    operations = [
        migrations.AlterField(
            model_name='lokafyer',
            name=b'thumbnail2',
            field=image_cropping.fields.ImageRatioField(b'photo', '150x150', hide_image_field=False, size_warning=False, allow_fullsize=True, free_crop=False, adapt_rotation=False, help_text=None, verbose_name='thumbnail2'),
            preserve_default=True,
        ),
    ]
