# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0019_auto_20150911_1443'),
    ]

    operations = [
        migrations.CreateModel(
            name='Tour2',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('time_of_day', models.CharField(default=b'3', max_length=25, null=True, choices=[(b'morning', b'Morning'), (b'afternoon', b'Afternoon'), (b'evening', b'Evening'), (b'other', b'Other')])),
                ('date', models.DateField(null=True)),
                ('date_submitted', models.DateTimeField(auto_now_add=True, null=True)),
                ('durations', models.IntegerField(null=True)),
                ('comment', models.TextField(default=b'', null=True)),
                ('price', models.IntegerField(null=True)),
                ('lokafyer', models.ManyToManyField(to='api.Lokafyer', through='api.TourLokafyer', blank=True)),
                ('request', models.ForeignKey(to='api.BookingRequest')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterField(
            model_name='tourlokafyer',
            name='tour',
            field=models.ForeignKey(to='api.Tour2', null=True),
            preserve_default=True,
        ),
        migrations.DeleteModel(
            name='Tour',
        ),
    ]
