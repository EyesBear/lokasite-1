# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0009_auto_20150706_2138'),
    ]

    operations = [
        migrations.AlterField(
            model_name='lokafyer',
            name='languages',
            field=models.CharField(default=b'', max_length=1255, null=True, verbose_name=b'Languages I Speak', help_text=b'Please describe level of proficiency: fluent, conversational or beginner'),
            preserve_default=True,
        ),
    ]
