# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0010_auto_20150706_2202'),
    ]

    operations = [
        migrations.CreateModel(
            name='BookingRequestLokafyer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('bookingrequest', models.ForeignKey(to='api.BookingRequest')),
                ('lokafyer', models.ForeignKey(to='api.Lokafyer')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.RemoveField(
            model_name='bookingrequest',
            name='first_lokafyer',
        ),
        migrations.RemoveField(
            model_name='bookingrequest',
            name='second_lokafyer',
        ),
        migrations.RemoveField(
            model_name='bookingrequest',
            name='third_lokafyer',
        ),
        migrations.AddField(
            model_name='bookingrequest',
            name='durations',
            field=models.IntegerField(null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='bookingrequest',
            name='lokafyerRelation',
            field=models.ManyToManyField(to='api.Lokafyer', through='api.BookingRequestLokafyer', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='bookingrequest',
            name='referral',
            field=models.TextField(default=b'', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='bookingrequest',
            name='source',
            field=models.TextField(default=b'', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='bookingrequest',
            name='whattoshow',
            field=models.TextField(default=b'', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='bookingrequest',
            name='group_type',
            field=models.CharField(default=b'0', max_length=25, choices=[(b'solo', b'Solo'), (b'family', b'Family'), (b'friends', b'Friends'), (b'collegues', b'Collegues'), (b'lovers', b'Lovers')]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='bookingrequest',
            name='time_of_day',
            field=models.CharField(default=b'3', max_length=25, choices=[(b'morning', b'Morning'), (b'afternoon', b'Afternoon'), (b'evening', b'Evening'), (b'other', b'Other')]),
            preserve_default=True,
        ),
    ]
