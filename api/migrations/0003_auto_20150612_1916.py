# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0002_auto_20150611_1050'),
    ]

    operations = [
        migrations.AddField(
            model_name='lokafyer',
            name='photo',
            field=models.ImageField(null=True, upload_to=b'form_photos'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='area',
            name='city',
            field=models.ForeignKey(related_name='areas', to='api.City'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='lokafyer',
            name='about',
            field=models.TextField(default=b'', help_text=b"This would appear on the profile's detailed view. Please include everything you would like travellers to know about you. The more detailed, the better (ex: what you study, what you do for a living, what you enjoy doing, etc.)", null=True, verbose_name=b'About me'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='lokafyer',
            name='areas',
            field=models.TextField(default=b'', help_text=b'Describe the areas of the city you are most familiar with', null=True, verbose_name=b'Often seen in:'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='lokafyer',
            name='enjoy_day',
            field=models.TextField(default=b'', null=True, verbose_name=b'On a sunny afternoon, I like to:'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='lokafyer',
            name='enjoy_night',
            field=models.TextField(default=b'', null=True, verbose_name=b'A perfect night out is:'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='lokafyer',
            name='languages',
            field=models.TextField(default=b'', help_text=b'Please describe level of proficiency: fluent, conversational or beginner', null=True, verbose_name=b'Languages I Speak'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='lokafyer',
            name='phrase',
            field=models.CharField(default=b'', max_length=256, null=True, verbose_name=b'How I would describe myself in one sentence'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='lokafyer',
            name='transportation',
            field=models.TextField(default=b'', null=True, verbose_name=b'How you get around '),
            preserve_default=True,
        ),
    ]
