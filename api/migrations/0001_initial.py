# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Area',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(default=b'', max_length=256, null=True)),
                ('description', models.TextField(default=b'', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='BookingRequest',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('first_name', models.CharField(default=b'', max_length=256, null=True)),
                ('last_name', models.CharField(default=b'', max_length=256, null=True)),
                ('email', models.EmailField(max_length=75)),
                ('group_size', models.IntegerField()),
                ('group_type', models.CharField(default=b'0', max_length=1, choices=[(b'0', b'Other'), (b'1', b'Friends'), (b'2', b'Family'), (b'3', b'Lovers'), (b'4', b'Collegues')])),
                ('time_of_day', models.CharField(default=b'3', max_length=1, choices=[(b'0', b'Morning'), (b'1', b'Afternoon'), (b'2', b'Evening'), (b'3', b'Other')])),
                ('date', models.DateField()),
                ('about', models.TextField(default=b'', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='City',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('city', models.CharField(default=b'', max_length=256, null=True)),
                ('province', models.CharField(default=b'', max_length=256, null=True)),
                ('country', models.CharField(default=b'', max_length=256, null=True)),
                ('description', models.TextField(default=b'', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Interest',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('category', models.CharField(default=b'', max_length=256, null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Language',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('level', models.CharField(default=b'B', max_length=1, choices=[(b'B', b'Beginner'), (b'I', b'Intermediate'), (b'F', b'Fluent')])),
                ('name', models.CharField(default=b'', max_length=256, null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Lokafyer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('about', models.TextField(default=b'', null=True)),
                ('address', models.CharField(default=b'', max_length=256, null=True)),
                ('age_range', models.CharField(default=b'5', max_length=1, choices=[(b'U', b'Under 25'), (b'2', b'25 - 35'), (b'3', b'35-45'), (b'4', b'45-60'), (b'O', b'Over 60'), (b'5', b'Not specified')])),
                ('alt_phone', models.CharField(blank=True, max_length=15, validators=[django.core.validators.RegexValidator(regex=b'^\\+?1?\\d{9,15}$', message=b"Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.")])),
                ('areas', models.TextField(default=b'', null=True)),
                ('availability', models.TextField(default=b'', null=True)),
                ('created_at', models.DateTimeField()),
                ('date_of_birth', models.DateTimeField()),
                ('email', models.EmailField(max_length=75)),
                ('enjoy_day', models.TextField(default=b'', null=True)),
                ('enjoy_night', models.TextField(default=b'', null=True)),
                ('first_name', models.CharField(default=b'', max_length=64, null=True)),
                ('gender', models.CharField(default=b'N', max_length=1, choices=[(b'M', b'Male'), (b'F', b'Female'), (b'N', b'Not Specified')])),
                ('languages', models.TextField(default=b'', null=True)),
                ('last_logon', models.DateTimeField()),
                ('last_name', models.CharField(default=b'', max_length=64, null=True)),
                ('phone', models.CharField(blank=True, max_length=15, validators=[django.core.validators.RegexValidator(regex=b'^\\+?1?\\d{9,15}$', message=b"Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.")])),
                ('photo_path', models.CharField(default=b'', max_length=256, null=True)),
                ('phrase', models.CharField(default=b'', max_length=256, null=True)),
                ('postal_code', models.CharField(default=b'', max_length=10, null=True)),
                ('standard_rate', models.IntegerField()),
                ('status', models.CharField(max_length=64)),
                ('transportation', models.TextField(default=b'', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='LokafyerArea',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('area', models.ForeignKey(to='api.Area')),
                ('lokafyer', models.ForeignKey(to='api.Lokafyer')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='LokafyerInterest',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('interest', models.ForeignKey(to='api.Interest')),
                ('lokafyer', models.ForeignKey(to='api.Lokafyer')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='LokafyerLanguage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('language', models.ForeignKey(to='api.Language')),
                ('lokafyer', models.ForeignKey(to='api.Lokafyer')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Payment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Tour',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('lokafyer', models.ForeignKey(to='api.Lokafyer')),
                ('request', models.ForeignKey(to='api.BookingRequest')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='lokafyer',
            name='areaRelation',
            field=models.ManyToManyField(to='api.Area', through='api.LokafyerArea'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='lokafyer',
            name='city',
            field=models.ForeignKey(to='api.City'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='lokafyer',
            name='interestRelation',
            field=models.ManyToManyField(to='api.Interest', through='api.LokafyerInterest'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='lokafyer',
            name='languageRelation',
            field=models.ManyToManyField(to='api.Language', through='api.LokafyerLanguage'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='bookingrequest',
            name='destination',
            field=models.ForeignKey(to='api.City'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='bookingrequest',
            name='first_lokafyer',
            field=models.ForeignKey(related_name='first_requested_lokafyer', to='api.Lokafyer'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='bookingrequest',
            name='second_lokafyer',
            field=models.ForeignKey(related_name='second_requested_lokafyer', to='api.Lokafyer'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='bookingrequest',
            name='third_lokafyer',
            field=models.ForeignKey(related_name='third_requested_lokafyer', to='api.Lokafyer'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='area',
            name='city',
            field=models.ForeignKey(to='api.City'),
            preserve_default=True,
        ),
    ]
