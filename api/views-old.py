from django.shortcuts import render
from django.http import HttpResponse
from django.core.exceptions import ObjectDoesNotExist
from django.views.decorators.csrf import csrf_exempt
import json
from models import City
from models import Lokafyer
from models import BookingRequest
from transactional_email.mandrill import send_email
from django.conf import settings

TIME_OF_DAY = ['Morning', 'Afternoon', 'Evening', 'Other']

def city_info(request, city):
	print 'request for city with name ' + city
	try:
		model = City.objects.get(city=city.capitalize())
		response_data  = model.to_json()
		return HttpResponse(json.dumps(response_data), content_type="application/json")
	except ObjectDoesNotExist:
		return HttpResponse("Lokafy isn't operating out of that city - yet. Visit www.lokafy.com/waitlist to register as a host or be notified when we begin.", content_type="text/plain")

def lokafyer_info(request, id):
	print 'request for guide with id ' + id
	return HttpResponse(json.dumps({'doesNotExist':'true', 'message': 'A Lokafyer with that request id does not exist.'}), content_type="application/json")

#@csrf_exempt
#def booking_request(request):
	#return HttpResponse(json.dumps({'status': 'success'}), content_type="application/json")
#	if request.method == 'POST':
#		data = request.POST
	#	# Create the model
	#	try:
	#		first_lok = Lokafyer.objects.get(id=data['firstLokafyer'])
	#		second_lok = Lokafyer.objects.get(id=data['secondLokafyer'])
	#		third_lok = Lokafyer.objects.get(id=data['thirdLokafyer'])
	#		city = City.objects.get(city=data['destination'].capitalize())
	#		group_choice = next(value for value, name in BookingRequest.GROUP_TYPE_CHOICES if name==data['groupType'])
	#		time_of_day = next(value for value, name in BookingRequest.TIME_OF_DAY_CHOICES if name==data['timeOfDay'])
	#		m = BookingRequest(
	#				first_name = data['firstName'],
	#				last_name = data['lastName'],
	#				email = data['email'],
	#				about = data['about'],
	#				destination = city,
	#				group_size = data['groupSize'],
	#				group_type = group_choice,
	#				date = data['date'],
	#				time_of_day = time_of_day,
	#				duration = data['duration'],
	#				first_lokafyer = first_lok,
	#				second_lokafyer = second_lok,
	#				third_lokafyer = third_lok
	#			)
	#		m.save()
		#except Exception:
		#	return HttpResponse(json.dumps({'status': 'failure'}), content_type="application/json")
		
		# Send the email
		#data['first_lokafyer'] = first_lok.first_name
		#data['first_id'] = first_lok.id
		#data['second_lokafyer'] = second_lok.first_name
		#data['second_id'] = second_lok.id
		#data['third_lokafyer'] = third_lok.first_name
		#data['third_id'] = third_lok.id
		
#		send_email(subject='Booking request from ' + 'zach' + ' ' + 'last_name', 
#					template_path='email/booking-request.html', 
#					dynamic_content=data,
#					from_email='info.lokafy@gmail.com',
#					from_name='Booking Service',
#					destination_email='zach.munrocape@gmail.com',
#					destination_name='Kiran')
		
		#send_email(subject='Lokafy booking confirmation',
		#			template_path='email/booking-receipt.html',
		#			dynamic_content=data,
		#			from_email=settings.BOOKING_EMAIL,
		#			from_name='Lokafy',
		#			destination_email=data['email'],
		#			destination_name=data['firstName'] + ' ' + data['lastName'])

		# Return the response
#		return HttpResponse(json.dumps({'status': 'success'}), content_type="application/json")
#	else:
#		return HttpResponse(json.dumps(None), content_type="application/json")
@csrf_exempt
def booking_request(request):
	#return HttpResponse(json.dumps({'status': 'success'}), content_type="application/json")
	#print 'in booking request'
	if request.method == 'POST':
		#print 'its a post'
		#print request.body
		data = json.loads(request.body)
		data['time_of_day'] = TIME_OF_DAY[int(data['time_of_day'])]
		print data
		if (len(data['lokafyers']) == 3):
			try:
				third_lok = Lokafyer.objects.get(id=data['lokafyers'][2])
				data['third_lokafyer'] = third_lok.first_name
				data['third_id'] = third_lok.id
			except Exception:
				print 'oops'
		if (len(data['lokafyers']) >= 2):
			try:
				second_lok = Lokafyer.objects.get(id=data['lokafyers'][1])
				data['second_lokafyer'] = second_lok.first_name
				data['second_id'] = second_lok.id
			except Exception:
				print 'oops'
		if (len(data['lokafyers']) >= 1):
			try:
				first_lok = Lokafyer.objects.get(id=data['lokafyers'][0])
				data['first_lokafyer'] = first_lok.first_name
				data['first_id'] = first_lok.id
			except Exception:
				print 'oops'
		#print data['chosenLokafyers']
		# {"first_name":"zach",
		# "last_name":"munrocape",
		# "email":"zach.munrocape@gmail.com",
		# "group_size":2,
		# "group_type":"",
		# "duration":4,
		# "time_of_day":"0",
		# "date":"",
		# "first_lokafyer":"",
		# "second_lokafyer":"",
		# "third_lokafyer":"",
		# "about":"lalala about me!",
		# "mobile_no":"9025551234"}
		#print request
	#	# Create the model
	#	try:
	#		first_lok = Lokafyer.objects.get(id=data['firstLokafyer'])
	#		second_lok = Lokafyer.objects.get(id=data['secondLokafyer'])
	#		third_lok = Lokafyer.objects.get(id=data['thirdLokafyer'])
	#		city = City.objects.get(city=data['destination'].capitalize())
	#		group_choice = next(value for value, name in BookingRequest.GROUP_TYPE_CHOICES if name==data['groupType'])
	#		time_of_day = next(value for value, name in BookingRequest.TIME_OF_DAY_CHOICES if name==data['timeOfDay'])
	#		m = BookingRequest(
	#				first_name = data['firstName'],
	#				last_name = data['lastName'],
	#				email = data['email'],
	#				about = data['about'],
	#				destination = city,
	#				group_size = data['groupSize'],
	#				group_type = group_choice,
	#				date = data['date'],
	#				time_of_day = time_of_day,
	#				duration = data['duration'],
	#				first_lokafyer = first_lok,
	#				second_lokafyer = second_lok,
	#				third_lokafyer = third_lok
	#			)
	#		m.save()
		#except Exception:
		#	return HttpResponse(json.dumps({'status': 'failure'}), content_type="application/json")
		
		# Send the email
		
		send_email(subject='Booking request from ' + data['first_name'] + ' ' + data['last_name'], 
					template_path='email/booking-request.html', 
					dynamic_content=data,
					from_email='booking@lokafy.com',
					from_name='Booking Service',
					destination_email='info.lokafy@gmail.com',
					destination_name='Kiran')
		
		#send_email(subject='Lokafy booking confirmation',
		#			template_path='email/booking-receipt.html',
		#			dynamic_content=data,
		#			from_email=settings.BOOKING_EMAIL,
		#			from_name='Lokafy',
		#			destination_email=data['email'],
		#			destination_name=data['firstName'] + ' ' + data['lastName'])

		# Return the response
		return HttpResponse(json.dumps({'status': 'success'}), content_type="application/json")
	else:
		return HttpResponse(json.dumps(None), content_type="application/json")

def booking_payment(request):
	if request.method == 'POST':
		return HttpResponse(json.dumps('success'), content_type="application/json")
	else:
		return HttpResponse(json.dumps('no get'), content_type="application/json")

	
@csrf_exempt
def contact(request):
	try:
		raw = request.body
		split = raw.split('&')
		d = {}
		for item in split:
			arr = item.split('=')
			print arr
			d[arr[0]] = arr[1]
		data = d
		data['message'].replace('+', ' ')
		send_email(subject='Contact form submission from ' + data['name'], 
					template_path='email/contact-form.html', 
					dynamic_content=data,
					from_email='info.lokafy@gmail.com',
					from_name='Contact Service',
					destination_email='info.lokafy@gmail.com',
					destination_name='Kiran')
		return HttpResponse(json.dumps('success'), content_type="application/json")
	except Exception:
		return HttpResponse(json.dumps('failure'), content_type="application/json")
