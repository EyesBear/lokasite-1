from django.shortcuts import render
from django.http import HttpResponse
from django.core.exceptions import ObjectDoesNotExist
from django.views.decorators.csrf import csrf_exempt
import json
import datetime
from models import City
from models import Lokafyer
from models import BookingRequest
from models import BookingRequestLokafyer
from models import LokafyTour
from models import LokafyerTour
from transactional_email.mandrill import send_email
from django.conf import settings
import stripe

TIME_OF_DAY = ['Morning', 'Afternoon', 'Evening', 'Other']

def city_info(request, city):
	print 'request for city with name ' + city
	try:
		model = City.objects.get(city=city.capitalize())
		response_data  = model.to_json()
		return HttpResponse(json.dumps(response_data), content_type="application/json")
	except ObjectDoesNotExist:
		return HttpResponse("Lokafy isn't operating out of that city - yet. Visit www.lokafy.com/waitlist to register as a host or be notified when we begin.", content_type="text/plain")



def lokafyer_info(request, id):
	print 'request for guide with id ' + id
	return HttpResponse(json.dumps({'doesNotExist':'true', 'message': 'A Lokafyer with that request id does not exist.'}), content_type="application/json")



@csrf_exempt
def booking_request(request):
	#return HttpResponse(json.dumps({'status': 'success'}), content_type="application/json")
	#print 'in booking request'
	if request.method == 'POST':
		data = json.loads(request.body)
		print 'before the if stripe'
		try:
			if 'stripeToken' in data:
				print 'in stripeTOken'
				stripe.api_key = "sk_live_v4u8rcCqLdIrni7u7eQSpyZn"

				# Get the credit card details submitted by the form
				token = data['stripeToken']
				print data['total']
				# Create the charge on Stripe's servers - this will charge the user's card
				try:
					if data['destination'] == 'Toronto':
						charge = stripe.Charge.create(
							amount=data['total']*100, # amount in cents, again
							currency="cad",
							source=token,
							description="Lokafyer charge")
										
					elif data['destination'] == 'Paris':
						charge = stripe.Charge.create(
							amount=data['total']*100, # amount in cents, again
							currency="eur",
							source=token,
							description="Lokafyer charge"
						)				  
					
				except stripe.error.CardError, e:
				  # The card has been declined
				  print e
				  data['paid'] = 'No'
				  data['comment'] = 'The card has been declined due to: ' + e

				if 'paid' not in data:
					data['paid'] = 'Yes';
				print data
				print 'after data'
			else:
				data['paid'] = 'No';

			city = City.objects.get(city=data['destination'].capitalize())
			m = BookingRequest(
					first_name = data['first_name'],
					last_name = data['last_name'],
					email = data['email'],
					about = data['about'],
					destination = city,
					group_size = data['group_size'],
					group_type = data['group_type'],
					date = data['date'],
					time_of_day = data['time_of_day'],
					durations = data['durations'],
					whattoshow = data['whattoshow'],
					source = data['source'],
					referral = data['referral'],
					price = data['total'],
					paid = data['paid'],
					comment = data['comment']
				)
			m.save()
			t = LokafyTour (	 
				request = m,
				time_of_day = data['time_of_day'],
				date = data['date'],
				durations=data['durations'],
				comment=data['comment'],
				price=data['total'])
			t.save()
			print 'after save'
			# first_lok = Lokafyer.objects.get(first_name=data['first_lokafyer'])
			for i in data['lokafyers']:
				p = BookingRequestLokafyer(bookingrequest = m, lokafyer = Lokafyer.objects.get(id=i))
				q = LokafyerTour(LokafyTour=t, city = city, lokafyer = Lokafyer.objects.get(id=i))
				# p.bookingrequest.add(m)
				# p.lokafyer.add(Lokafyer.objects.get(id_=data['lokafyers'][0]))
				print 'after setting up p'
				p.save()
				q.save()
			print 'after p is saved'

			# To booking
			print 'before set date'
			#data['date'] = datetime.datetime.strptime(data['date'], '%m/%d/%Y').strftime('%B %d %y')
		
			if (data['email'] != ''):
				send_email(subject='Booking request from ' + data['first_name'] + ' ' + data['last_name'], 
							template_path='email/booking-request.html', 
							dynamic_content=data,
							from_email=data['email'],
							from_name=data['first_name'] + ' ' + data['last_name'],
							destination_email='info.lokafy@gmail.com',
							destination_name='Kiran')

				send_email(subject='Booking request from ' + data['first_name'] + ' ' + data['last_name'],
							template_path='email/booking-request.html',
							dynamic_content=data,
							from_email=data['email'],
							from_name=data['first_name'] + ' ' + data['last_name'],
							destination_email='booking@lokafy.com',
							destination_name='Kiran')

			# Receipt to client

				print 'after sent today email'
				send_email(subject='Lokafy booking confirmation',
							template_path='email/booking-receipt.html',
							dynamic_content=data,
							from_email='booking@lokafy.com',
							from_name='Lokafy',
							destination_email=data['email'],
							destination_name=data['first_name'] + ' ' + data['last_name'])
				

				print 'after send 2nd email'
		except Exception as e:
			print type(e)
			print e.args
			print e
			return HttpResponse(json.dumps({'status': 'failure'}), content_type="application/json")
		return HttpResponse(json.dumps({'status': 'success'}), content_type="application/json")
	else:
		return HttpResponse(json.dumps(None), content_type="application/json")

def booking_payment(request):
	if request.method == 'POST':
		return HttpResponse(json.dumps('success'), content_type="application/json")
	else:
		return HttpResponse(json.dumps('no get'), content_type="application/json")

@csrf_exempt
def contact(request):
	try:
		message = json.loads(request.body)
		success = send_email(subject='Contact form submission from ' + message['name'], 
					template_path='email/contact-form.html', 
					dynamic_content=message,
					from_email=message['email'],
					from_name='Contact Service',
					destination_email='info.lokafy@gmail.com',
					destination_name='Kiran')
		return HttpResponse(json.dumps({'status': 'success'}), content_type="application/json")
	except Exception:
		return HttpResponse(json.dumps({'status': 'failure'}), content_type="application/json")
	

