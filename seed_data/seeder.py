# This file creates the fixtures that will be used to seed the database
import csv
import json
from datetime import datetime
MASTER_STRING = '[\n'
MASTER_FIRST = True
# Lokafyers
with open('lokafyer.csv', 'rb') as lokafyer_csv:
	next(lokafyer_csv)
	next(lokafyer_csv)
	lokafyer_reader = csv.reader(lokafyer_csv, delimiter=',', quotechar='"')
	for row in lokafyer_reader:
		if not MASTER_FIRST:
			MASTER_STRING = MASTER_STRING + ',\n'
		else:
			MASTER_FIRST = False
		lokafyer_json = {}
		lokafyer_json['pk'] = row[0]
		lokafyer_json['model'] = 'api.Lokafyer'
		fields = {}
		fields['city'] = row[1]
		fields['created_at'] = row[2]
		date_object = datetime.strptime(fields['created_at'], '%d-%m-%Y %H:%M')
		fields['created_at'] = str(date_object) + '+00:00'
		fields['last_logon'] = row[3]
		fields['last_logon'] = str(datetime.now()) + '+00:00'
		fields['status'] = row[4]
		fields['first_name'] = row[5]
		fields['last_name'] = row[6]
		fields['email'] = row[7]
		fields['phone'] = row[8]
		fields['alt_phone'] = row[9]
		fields['photo_path'] = row[10]
		fields['address'] = row[11]
		fields['postal_code'] = row[12]
		fields['standard_rate'] = 35
		fields['phrase'] = row[14]
		fields['gender'] = row[15][0]
		fields['age_range'] = row[16][0]
		fields['date_of_birth'] = str(datetime.now()) + '+00:00'
		fields['languages'] = row[18]
		fields['availability'] = row[19]
		fields['areas'] = row[20]
		fields['enjoy_day'] = row[21]
		fields['enjoy_night'] = row[22]
		fields['transportation'] = row[23]
		fields['about'] = row[24]
		lokafyer_json['fields'] = fields
		MASTER_STRING = MASTER_STRING + json.dumps(lokafyer_json, sort_keys=True, indent=4, separators=(',', ': '))
	MASTER_STRING = MASTER_STRING + '\n'
MASTER_STRING += ','
# Cities
with open('city.csv', 'rb') as city_csv:
	next(city_csv)
	city_reader = csv.reader(city_csv, delimiter=',', quotechar='"')
	city_data = '[\n'
	first = True
	for row in city_reader:
		if not first:
			city_data = city_data + ',\n'
		else:
			first = False
		model = {'pk': row[0], 'model': 'api.City'}
		city_json = {}
		city_json['city'] = row[1]
		city_json['province'] = row[2]
		city_json['country'] = row[3]
		city_json['description'] = row[4]
		model['fields'] = city_json
		MASTER_STRING = MASTER_STRING + json.dumps(model, sort_keys=True, indent=4, separators=(',', ': ')) + ','
	MASTER_STRING = MASTER_STRING  + '\n'

# Interests
with open('interest.csv', 'rb') as interest_csv:
	next(interest_csv)
	interest_reader = csv.reader(interest_csv, delimiter=',', quotechar='"')
	interest_data = '[\n'
	first = True
	for row in interest_reader:
		if not first:
			interest_data = interest_data + ',\n'
		else:
			first = False
		model = {'pk': row[0], 'model': 'api.Interest', 'fields': {'category': row[1]}}
		MASTER_STRING = MASTER_STRING + json.dumps(model, sort_keys=True, indent=4, separators=(',', ': ')) + ','
	MASTER_STRING = MASTER_STRING + '\n'

# Languages
with open('language.csv', 'rb') as language_csv:
	next(language_csv)
	language_reader = csv.reader(language_csv, delimiter=',', quotechar='"')
	output = '[\n'
	first = True
	for row in language_reader:
		if not first:
			output = output + ',\n'
		else:
			first = False
		model = {'pk': row[0], 'model': 'api.Language', 'fields': {'name': row[1], 'level': row[2][0]}}
		MASTER_STRING = MASTER_STRING + json.dumps(model, sort_keys=True, indent=4, separators=(',', ': ')) + ','
	MASTER_STRING = MASTER_STRING + '\n'

# City area
with open('area.csv', 'rb') as area_csv:
	next(area_csv)
	area_reader = csv.reader(area_csv, delimiter=',', quotechar='"')
	for row in area_reader:
		model = {'pk': row[0], 'model': 'api.Area', 'fields': {'city': row[1], 'name': row[2], 'description': row[3]}} 
		MASTER_STRING = MASTER_STRING + json.dumps(model, sort_keys=True, indent=4, separators=(',', ': '))  + ','
	MASTER_STRING = MASTER_STRING + '\n'

# Lokafy-Interest relationship
with open('lokafyer-interest.csv', 'rb') as interest_map_csv:
	next(interest_map_csv)
	interest_map_reader = csv.reader(interest_map_csv, delimiter=',', quotechar='"')
	output = '[\n'
	first = True
	for row in interest_map_reader:
		if not first:
			output = output + ',\n'
		else:
			first = False
		model = {'model': 'api.LokafyerInterest', 'fields': {'lokafyer': row[0], 'interest': row[1]}}
		MASTER_STRING = MASTER_STRING + json.dumps(model, sort_keys=True, indent=4, separators=(',', ': '))  + ','
	MASTER_STRING = MASTER_STRING + '\n'

# Lokafy-Area relationship
with open('lokafyer-area.csv', 'rb') as area_map_csv:
	next(area_map_csv)
	area_map_reader = csv.reader(area_map_csv, delimiter=',', quotechar='"')
	output = '[\n'
	first = True
	for row in area_map_reader:
		if not first:
			output = output + ',\n'
		else:
			first = False
		model = {'model': 'api.LokafyerArea', 'fields': {'lokafyer': row[0], 'area': row[1]}}
		MASTER_STRING = MASTER_STRING + json.dumps(model, sort_keys=True, indent=4, separators=(',', ': ')) + ','
	MASTER_STRING = MASTER_STRING + '\n'

# Lokafy-Language relationship
with open('lokafyer-language.csv', 'rb') as language_map_csv:
	next(language_map_csv)
	language_map_reader = csv.reader(language_map_csv, delimiter=',', quotechar='"')
	output = '[\n'
	first = True
	for row in language_map_reader:
		if not first:
			output = output + ',\n'
		else:
			first = False
		model = {'model': 'api.LokafyerLanguage', 'fields': {'lokafyer': row[0], 'language': row[1]}}
		MASTER_STRING = MASTER_STRING + json.dumps(model, sort_keys=True, indent=4, separators=(',', ': ')) + ','
	MASTER_STRING = MASTER_STRING + '\n'

f = open('../api/fixtures/all_fixture.json', 'w')
f.write(MASTER_STRING)
f.close()














