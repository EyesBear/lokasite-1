from django.shortcuts import render
from django.core.exceptions import ObjectDoesNotExist
from django.views.generic.edit import CreateView
from api.models import City, Lokafyer, LokafyerArea
from django import forms
from django.shortcuts import render_to_response
from django.forms.models import modelform_factory
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.core.urlresolvers import reverse
import json


class ModelFormWidgetMixin(object):
    def get_form_class(self):
        return modelform_factory(self.model, fields=self.fields, widgets=self.widgets)


def index(request):
	return render(request, 'pages/index.html', {})

def about(request):
	return render(request, 'pages/about.html', {})

def faqs(request):
	return render(request, 'pages/faqs.html', {})

def mission(request):
	return render(request, 'pages/mission.html', {})

def pricing(request):
	return render(request, 'pages/pricing.html', {})

def thanks(request):
	return render(request, 'pages/thanks.html', {})

def pricing_paris(request):
	return render(request, 'pages/pricing-paris.html', {})

def pricing_toronto(request):
	return render(request, 'pages/pricing-toronto.html', {})

def reviews(request):
	return render(request, 'pages/reviews.html', {})

def contact(request):
	return render(request, 'pages/contact.html', {})

def values(request):
	return render(request, 'pages/values.html', {})

def city(request, city):
	print 'request for city with name ' + city
	try:
		city = City.objects.get(city=city.capitalize())
		lokafyer = Lokafyer.objects.get(id=1)
		lokafyerArea = LokafyerArea
		json = city.to_json()
		dic={}
		for area in city.areas.all():
			for loka in json['lokafyers']:
				if str(area.code) in loka['areas']:
					if loka['status'] != 'Deactivated':
						if area.code in dic:
							dic[str(area.code)].append([str(loka['thumbnail_url']),str(loka['id'])])
						else:
							dic[str(area.code)]=[[str(loka['thumbnail_url']),str(loka['id'])]]
	#	dic = json['lokafyers'][0]['areas']
                return render(request, 'pages/lokafyer.html', {'city_obj': city, 'city': city.city, 'lokafyerr': json['lokafyers'] ,'dic': dic, 'lokafyers': [1,2,3], 'lokafyerArea': lokafyerArea, 'data': json})
	except ObjectDoesNotExist:
		return render(request, 'pages/waitinglist.html', {})

@csrf_exempt
def booking_form(request):

	return render(request, 'pages/booking_request.html')


# @csrf_exempt
# def booking_requestform(request):
# 	print 'inside requerstform!!!!!!!!!!!!!!!!!!!!!!'
# 	if request.method == 'POST':
# 		print 'inide post!!!!!!!!!!!'
# 		#data['time_of_day'] = TIME_OF_DAY[int(data['time_of_day'])]
		
# 		print 'inthere'
# 		#return HttpResponse(json.dumps({'status': 'success'}), content_type="application/json")
# 		url = reverse('/booking_form')
# 		return redirect('booking_form')
# 	else:
# 		print 'in elseeeeeeeeeeeeee'
# 		return HttpResponse(json.dumps(None), content_type="application/json")

class PersonCreate(ModelFormWidgetMixin, CreateView):
	model = Lokafyer
	template_name = 'pages/person-create.html'
	fields = [
		'city',
		'first_name', 'last_name', 'date_of_birth', 'email', 'phone', 'age_range', 'photo', 'phrase',
		'languages', 'areas', 'enjoy_day', 'enjoy_night', 'transportation', 'availability', 'about',
	]
	widgets = {
		'enjoy_night':  forms.Textarea(attrs={'rows':6}),
		'enjoy_day':  forms.Textarea(attrs={'rows':6}),
		'languages':  forms.Textarea(attrs={'rows':6}),
		'transportation':  forms.Textarea(attrs={'rows':6}),
		'areas':  forms.Textarea(attrs={'rows':6}),
		
	}
	success_url = '/thanks/'
	

person_create = PersonCreate.as_view()
