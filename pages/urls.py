from django.conf.urls import patterns, include, url
from django.contrib import admin
from pages import views

urlpatterns = patterns('',
    url(r'^$', 'pages.views.index', name='index'),
    url(r'^about/', 'pages.views.about', name='about'),
    url(r'^city/(?P<city>\w+)/', 'pages.views.city', name='city'),
    url(r'^api/', include('api.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^payment/pay', 'payment.views.pay', name='pay'),
    url(r'^booking_form/', 'pages.views.booking_form', name='booking_form'),

)
