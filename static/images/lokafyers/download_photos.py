import urllib

if __name__ == "__main__":
	f = open('photopaths.txt', 'r')
	lines = f.readlines()
	for i in xrange(0, len(lines), 2):
		person_id = lines[i]
		photo_path = lines[i+1]
		if photo_path.rstrip() != '':
			print 'requesting photo at ' + photo_path.rstrip()
			urllib.urlretrieve(photo_path.rstrip(), person_id.rstrip() + ".jpg")
