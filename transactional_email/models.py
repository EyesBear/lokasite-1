from django.db import models

class Communication(models.Model):
	'''Maintains a record of all transactional emails'''

	recipient = models.EmailField(default='', null=True)
	subject = models.CharField(default='', max_length=256, null=True)
	date = models.DateField(null=True)
	content = models.TextField(default='', null=True)
	success = models.BooleanField(default=True)