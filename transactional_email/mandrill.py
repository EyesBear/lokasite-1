import os
from datetime import datetime    
from .models import Communication
from mailsnake import MailSnake
from django.template.loader import get_template
from django.template import Context
from mailsnake.exceptions import *

api_key = 'ugilGVKKZJE4pRgDKzTd5g'
mapi = MailSnake(api_key, api='mandrill')

def send_email(subject, template_path, dynamic_content, from_email, from_name, destination_email, destination_name): #, destination_email, destination_name, from_email, from_name, subject, ):
	
	# Create the dynamic template
	html = get_template(template_path)
	d = Context(dynamic_content)
	content = html.render(d)
	
	# Send the message
	success = mapi.messages.send(message={'html':content, 
								'subject':subject, 
								'from_email':from_email, 
								'from_name':from_name, 
								'to':[{'email':destination_email, 
										'name':destination_name
									}]
								})
	success = (success == 'PONG!')
	
	# Record a copy of the message
	record = Communication(recipient=destination_email, subject=subject, date=datetime.now(), content=content, success=success)
	record.save()
	return True
