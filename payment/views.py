from django.shortcuts import render
from django.shortcuts import redirect

# Create your views here.
def pay(request):
	if request.method != 'POST':
		return redirect('/')
	
	return redirect('city/toronto')